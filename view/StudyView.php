<?php

require_once 'lib/View.php';

class StudyView extends View
{
    function __construct()
    {
        parent::__construct();
//        echo 'En la vista Index<br>';
    }

    public function render($rows, $template='study.tpl')
    {
//        $this->smarty->assign('method', $this->getMethod());
        
        $this->smarty->assign('rows', $rows);
        $this->smarty->display($template);
    }

    public function prueba()
    {
        $template='studyPrueba.tpl';
        $js[] = 'prueba.js';
        $js[] = 'prueba2.js';
        $this->smarty->assign('js', $js);
        $this->smarty->display($template);
    }
    public function add( $error="", $levelRows)
    {
        $template='studyFormAdd.tpl';
        $this->smarty->assign('levelRows', $levelRows);
        $this->smarty->display($template);
    }
    
    public function edit($row, $error="", $levelRows)
    {
        $template='studyFormEdit.tpl';
        $this->smarty->assign('row', $row);
        $this->smarty->assign('levelRows', $levelRows);
        $this->smarty->assign('error', $error);
        $this->smarty->display($template);
    }
    
    public function plan($levels)
    {
        $template='studyPlan.tpl';
        $js[] = 'ajaxPlan.js';
        $this->smarty->assign('js', $js);
        $this->smarty->assign('levels', $levels);
//        $this->smarty->assign('rows', $rows);
        $this->smarty->display($template);
    }    
    
    public function studyLevelOption($rows)
    {
        $echo = '';
        foreach ($rows as $row) {
            $echo .= "<option value=$row[id]>$row[nombre]</option>";
        }        
        echo $echo;        
    }
}
