<?php
require_once 'lib/Controller.php';
require_once 'model/LevelModel.php';

class Study extends Controller{
    private $_levelModel = LevelModel;

    function __construct()
    {
        parent::__construct('Study');
        $this->_levelModel = new LevelModel;
    }

   
    public function index()
    {
        //mostrar lista de todos los registros.
        $rows = $this->model->getAll();
        $this->view->render($rows);
    }
    
//    *********************************************
//    PRUEBA CON AJAX
    public function prueba()
    {
        $this->view->prueba();
    }
    
    public function ajaxPrueba($param)
    {
        echo 'Vamos a mostrar este texto con Ajax ' . $param;
    }
//    *********************************************
    public function plan()
    {
//        echo 1;
        $levelModel = new LevelModel();
        $levels = $levelModel->getAll();
//        var_dump($levels);
        $this->view->plan($levels);
    }
    
    public function ajaxStudyList(){
//        var_dump($_POST);exit();
        $rows = $this->model->getFromLevel($_POST['idLevel']);
        $this->view->studyLevelOption($rows);
    }
    
    
    public function ajaxStudyList2(){
//        var_dump($_POST);exit();
        $rows = $this->model->getFromLevel($_POST['idLevel']);
        echo json_encode($rows);
    }
    
    
    public function add($error="")
    {
        $levelRows = $this->_levelModel->getAll();
        $this->view->add($error, $levelRows);
    }
    
    public function insert()
    {
        $row = $_POST;  
        $row['password'] = md5($row['password']);
        $this->model->insert($row);    
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/study/index');
    }
    public function delete($id)
    {
        $this->model->delete($id);    
        header('Location: ' . Config::URL . $_SESSION['lang'] . '/study/index');
    }
    
    public function edit($id, $error="")
    {
        $row = $this->model->get($id);
        $levelRows = $this->_levelModel->getAll();
        $this->view->edit($row, $error, $levelRows);
    }

    public function update()
    {
        $row = $_POST; 
        $error = $this->_validate($row);
        if (count($error)){
            $this->edit($row['id'], $error);
        }
        else{
            $row['password'] = md5($row['password']);
            $this->model->update($row);    
            header('Location: ' . Config::URL . $_SESSION['lang'] . '/study/index');
        }
    }
    
    private function _validate($row)
    {
        $error = array();
               
//        if (!preg_match("/^.{6,20}$/", $row['password'])){
//            $error['password'] = 'error_password';
//        }
        
        return $error;
    }
    
}