<!DOCTYPE html>

<html>
    <head>
        <title>TODO: supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{$url}public/css/default.css" />
        <script src="{$url}public/js/jquery.js" type="text/javascript"></script>
        {if isset($js) && count($js)}
        {foreach item=file from=$js}        
        <script src="{$url}public/js/{$file}" type="text/javascript"></script>        
        {/foreach}   
        {/if}
         
    </head>
    <body>
        <div id="header">
            <div id="title">
                Aplicación de trabajo Tema 7 - {$language->translate('language')}
            </div>
            <div  style="float: left">
                <a href="{$url}{$lang}/index" >{$language->translate('index')}</a> 
                <a href="{$url}{$lang}/help" >{$language->translate('help')}</a>
                <a href="{$url}{$lang}/user" >{$language->translate('user')}</a>
                <a href="{$url}{$lang}/study" >{$language->translate('study')}</a>
            </div>
            <div  style="float: left ; padding-left:4em">
                <a href="{$url}/es/index" >ES</a> 
                <a href="{$url}/en/index" >EN</a>
            </div>
        </div>

            
