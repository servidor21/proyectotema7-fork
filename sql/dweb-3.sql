-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-02-2016 a las 17:03:56
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `dweb`
--
CREATE DATABASE IF NOT EXISTS `dweb` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `dweb`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

DROP TABLE IF EXISTS `alumno`;
CREATE TABLE IF NOT EXISTS `alumno` (
  `numero` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidos` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edad` int(2) DEFAULT NULL,
  `fechaMatricula` date NOT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=105 ;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`numero`, `nombre`, `apellidos`, `edad`, `fechaMatricula`) VALUES
(2, 'MarÃ­a', 'SÃ¡nchez', 23, '2015-05-02'),
(10, 'Mariano', 'Cabeza', 33, '2010-11-30'),
(16, 'Pepe', 'afddafd', 23, '0000-00-00'),
(17, 'Mario', 'Bavarian', 13, '2015-09-01'),
(18, 'Pepe', 'Navarro Sola', 39, '0000-00-00'),
(19, 'Mario', 'Bavarian', 13, '2011-09-01'),
(102, 'Raquel', 'Roma GÃ³mez', 31, '2010-11-30'),
(104, 'Daniel', 'Villaverde PÃ©rez', 5, '2000-05-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudio`
--

DROP TABLE IF EXISTS `estudio`; 
CREATE TABLE IF NOT EXISTS `estudio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codInterno` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `idNivel` int(11) DEFAULT NULL,
  `codOficial` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codInterno` (`codInterno`),
  KEY `idNivel` (`idNivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=113 ;

--
-- Volcado de datos para la tabla `estudio`
--

INSERT INTO `estudio` (`id`, `codInterno`, `nombre`, `idNivel`, `codOficial`) VALUES
(3, 'DAM', 'Desarrollo de apllicaciones Multiplatafo', 5, 'IFC302'),
(4, 'DAW', 'Desarrollo de aplicaciones web', 5, 'IFC303'),
(6, 'GMIF', 'Sistemas MicroinformÃ¡ticos y redes', 4, 'IFC201'),
(7, 'GMBB', 'Grado medio basura cuadrado', 2, 'xxx000'),
(34, 'GSDP', 'ESTILISMO Y DIRECCION DE PELUQUERIA', 5, '1MP303'),
(35, 'ADG', 'GESTION ADMINISTRATIVA', 4, 'ADG201'),
(36, 'FPB8', 'FPB - PELUQUERÍA Y ESTÉTICA', 3, 'FPB008'),
(37, 'GMCO', 'COMERCIO', 4, 'COM201'),
(38, 'GMEB', 'ESTETICA Y BELLEZA', 5, 'IMP202'),
(39, 'GMGA', 'GESTION ADMINISTRATIVA', 4, 'ADM201'),
(40, 'GMPL', 'PELUQUERIA Y COSMETICA CAPILAR', 5, 'IMP203'),
(41, 'GSAD', 'ADMINISTRACION Y FINANZAS', 5, 'ADG301'),
(42, 'GSAF', 'ADMINISTRACION Y FINANZAS', 5, 'ADM301'),
(43, 'GSCM', 'GESTION COMERCIAL Y MARKETING', 5, 'COM302'),
(44, 'GSEI', 'ESTÉTICA INTEGRAL Y BIENESTAR', 5, 'IMP302'),
(45, 'GSIP', 'ASESORIA DE IMAGEN PERSONAL', 5, 'IMP301');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

DROP TABLE IF EXISTS `materia`;
CREATE TABLE IF NOT EXISTS `materia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=166 ;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`id`, `codigo`, `nombre`) VALUES
(1, '0156', 'Inglés'),
(2, '0179', 'Inglés'),
(3, '0373', 'Lenguajes de marcas y Sistemas'),
(4, '0437', 'Comunicación empresarial'),
(5, '0438', 'Op. Adm. de compra-venta'),
(6, '0439', 'Empresa y Administración'),
(7, '0440', 'Trat Informat de la informac'),
(8, '0441', 'Técnica contable'),
(9, '0442', 'Operac. adm. Recursos Humanos'),
(10, '0443', 'Tratamiento de la Documenación'),
(11, '0446', 'Empresa en el Aula'),
(12, '0448', 'Oper. auxiliares de Gestión'),
(13, '0449', 'Formac y orientación laboral'),
(14, '0451', 'Formación Centros de Trabajo'),
(15, '0483', 'Sistemas Informáticos'),
(16, '0484', 'Bases de datos'),
(17, '0485', 'Programación'),
(18, '0486', 'Acceso a datos'),
(19, '0487', 'Entornos de Desarrollo'),
(20, '0488', 'Desarrollo de interfaces'),
(21, '0489', 'Progr  multimedia disp móviles'),
(22, '0490', 'Prog de servicios y procesoso'),
(23, '0491', 'Sistemas de gestión empresaria'),
(24, '0492', 'Proyecto de desarrollo'),
(25, '0493', 'Formación y Orientación Labora'),
(26, '0494', 'Empresa e iniciativa emprended'),
(27, '0495', 'Formación en centros de Trabaj'),
(28, '0612', 'Des web en entorno cliente'),
(29, '0613', 'Des web en entorno servidor'),
(30, '0614', 'Despliegue de aplicaciones web'),
(31, '0615', 'Diseño de interfaces Web'),
(32, '0616', 'Proyecto de desarrollo de Apli'),
(33, '0617', 'Formación y Orientación Labor'),
(34, '0618', 'Empresa e iniciativa emprended'),
(35, '0619', 'Formación en centros de trabaj'),
(36, '0633', 'Técnicas de higiene facial'),
(37, '0634', 'Maquillaje'),
(38, '0635', 'Depilación mecánica y decolora'),
(39, '0636', 'Estética de manos y pies'),
(40, '0637', 'Técnicas de uñas artificiales'),
(41, '0638', 'Analisis estético'),
(42, '0639', 'Actividades básicas en cabina'),
(43, '0640', 'Imagen corporal y hab saludab'),
(44, '0641', 'Cosmetología para Estética y B'),
(45, '0642', 'Perfumería y cosmética natural'),
(46, '0643', 'Marketing y venta en Imag Pers'),
(47, '0644', 'Formación y Orientación Labor'),
(48, '0645', 'Empresa e iniciativa emprended'),
(49, '0646', 'Formación en Centros de Trabaj'),
(50, '0647', 'Gest de la document juridica'),
(51, '0648', 'Recursos humanos y responsabil'),
(52, '0649', 'Ofimática y proceso de la inf'),
(53, '0650', 'Proceso integral de la activ'),
(54, '0651', 'Comunicación y atención al cli'),
(55, '0652', 'Gestión de recursos humanos'),
(56, '0653', 'Gestión financiera'),
(57, '0654', 'Contabilidad y Fiscalidad'),
(58, '0655', 'Gestión logistica y comercial'),
(59, '0656', 'Simulación empresarial'),
(60, '0657', 'Proyecto de administración y F'),
(61, '0658', 'Formación y Orientación labora'),
(62, '0660', 'Formación en Centros de Trabaj'),
(63, '0744', 'Aparatología estética'),
(64, '0745', 'Estética hidrotermal'),
(65, '0746', 'Depilación avanzada'),
(66, '0747', 'Masaje estético'),
(67, '0748', 'Drenaje estético y tecnicas'),
(68, '0749', 'Micropigmentación'),
(69, '0750', 'Procesos fisiologócos y de hig'),
(70, '0751', 'Dermoestética'),
(71, '0752', 'Cosmética aplicada a Estética'),
(72, '0753', 'Tratamientos esteticos integr'),
(73, '0754', 'proyecto de estetica y bienest'),
(74, '0755', 'Formación y orientación labora'),
(75, '0756', 'Empresa e iniciativa emprend'),
(76, '0757', 'Formación en centros de trab'),
(77, '0842', 'Peinados y recogidos'),
(78, '0843', 'Coloración capilar'),
(79, '0844', 'Cosmética para Peluquería'),
(80, '0845', 'Tecnicas de corte del cabello'),
(81, '0846', 'Cambios de forma permanente en'),
(82, '0848', 'Peluquería y estilismo mascul'),
(83, '0849', 'Análisis Capilar'),
(84, '0851', 'Formación y orientación labora'),
(85, '0852', 'Empresa e iniciativa emprend'),
(86, '0853', 'Formacion en centros de trabaj'),
(87, '1064', 'Dermotricología'),
(88, '1065', 'Recursos técnicos y cosméticos'),
(89, '1066', 'Tratamientos Capilares'),
(90, '1067', 'Procedimientos y técnicas pelu'),
(91, '1068', 'Peinados para producciones aud'),
(92, '1069', 'Estilismo en Peluquería'),
(93, '1070', 'Estudio de la Imagen'),
(94, '1071', 'Dirección y comercialización'),
(95, '1072', 'Peluquería en cuidados espec'),
(96, '1073', 'Proyecto de estilismo y direc'),
(97, '1074', 'Formación y Orientación lab'),
(98, '1075', 'Empresa e iniciativa emprended'),
(99, '1076', 'Formación en centros de trabaj'),
(100, '3005', 'Atención al cliente'),
(101, '3009', 'Ciencias aplicadas I'),
(102, '3011', 'Comunicación y sociedad I'),
(103, '3012', 'Comunicación y sociedad II'),
(104, '3042', 'Ciencias aplicadas II'),
(105, '3060', 'Preparación del entorno prof'),
(106, '3061', 'Cuidados estéticos básic uñas'),
(107, '3062', 'Depilación mecánica y decolor'),
(108, '3063', 'Maquillaje'),
(109, '3064', 'Lavado y cambios de forma'),
(110, '3065', 'Cambio de color'),
(111, '3067', 'Formación en centros de trab'),
(112, 'A050', 'Lengua Extr Prof: Inglés1'),
(113, 'A051', 'Lengua Extranjera: inglés II'),
(114, 'A052', 'Lengua extran prof: Inglés I'),
(115, 'A053', 'Leng Extranj prof: Inglés 2'),
(116, 'A073', 'Lengua Extr Prof: Inglés1'),
(117, 'A074', 'Lengua Extranjera. Inglés II'),
(118, 'A083', 'Lengua extranj. entorno prof:'),
(119, 'A084', 'Len extranj prof: inglés 2'),
(120, 'ADPE', 'Adm. Gest. y Comercialización'),
(121, 'ADPU', 'Administración Pública'),
(122, 'ANPV', 'Animación del Punto de Venta'),
(123, 'APEC', 'Adm. y Gest. Peq. Establ. Com.'),
(124, 'APIC', 'Aplicaciones Informáticas (CM)'),
(125, 'APIN', 'Aplic. Informáticas'),
(126, 'APLC', 'Aplic. inf. prop. general'),
(127, 'APOF', 'Aplicaciones ofimáticas'),
(128, 'ASBE', 'Asesoría de Belleza'),
(129, 'AUDI', 'Auditoría'),
(130, 'AWEB', 'Aplicaciones Web'),
(131, 'CONT', 'Contabilidad'),
(132, 'EIEP', 'Empresa e iniciativa emprend.'),
(133, 'EMBE', 'Técnicas de embellecimiento'),
(134, 'ESTI', 'Estilismo en el Vestir'),
(135, 'FCT', 'Formación en Centro de Trabajo'),
(136, 'FOL', 'F.O.L.'),
(137, 'GCV', 'Gestión Compra-Venta'),
(138, 'GEAP', 'Gestión Aprovisionamento'),
(139, 'GECO', 'Gest. Comercial y att. cliente'),
(140, 'GEFI', 'Gestión Financiera'),
(141, 'GESC', 'Gestión compraventa'),
(142, 'GPER', 'Gest. Personal'),
(143, 'GPUB', 'Gest. Adm. Pública'),
(144, 'IMCO', 'Imagen Personal y Comun.'),
(145, 'INCO', 'Investigación comercial'),
(146, 'INGL', 'Inglés'),
(147, 'INPF', 'Lengua extr. prof: inglés 2'),
(148, 'INPR', 'Lengua extr. prof: inglés 1'),
(149, 'LOGC', 'Logística comercial'),
(150, 'MAPU', 'Marketing en el punto de venta'),
(151, 'MMEQ', 'Montaje y manteni de equipos'),
(152, 'OPAL', 'Operaciones de Almacenaje'),
(153, 'OPVT', 'Operaciones de Venta'),
(154, 'POMA', 'Políticas de Marketing'),
(155, 'PREM', 'Proyecto Empresarial'),
(156, 'PRFI', 'Productos Financieros'),
(157, 'PROT', 'Protocolo y Usos Sociales'),
(158, 'REHU', 'Recursos Humanos'),
(159, 'RELA', 'Relaciones en Entorno Trabajo'),
(160, 'RELO', 'Redes Locales'),
(161, 'SERV', 'Servicios en Red'),
(162, 'SGIN', 'Seguridad informática'),
(163, 'SIOM', 'Sistemas operativos monopuesto'),
(164, 'SIOR', 'Sistemas operativos en red'),
(165, 'TECL', 'Operatoria Teclados');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel`
--

DROP TABLE IF EXISTS `nivel`;
CREATE TABLE IF NOT EXISTS `nivel` (
  `id` int(11) NOT NULL,
  `nivel` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `nivel`
--

INSERT INTO `nivel` (`id`, `nivel`) VALUES
(1, 'Secundaria Obligatoria'),
(2, 'FP Básica'),
(3, 'Bachillerato'),
(4, 'C.F. Grado Medio'),
(5, 'C.F. Grado Superior');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan`
--

DROP TABLE IF EXISTS `plan`;
CREATE TABLE IF NOT EXISTS `plan` (
  `idEstudio` int(11) NOT NULL DEFAULT '0',
  `idMateria` int(11) NOT NULL DEFAULT '0',
  `curso` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idEstudio`,`idMateria`,`curso`),
  KEY `idMateria` (`idMateria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `plan`
--

INSERT INTO `plan` (`idEstudio`, `idMateria`, `curso`) VALUES
(35, 1, 1),
(35, 1, 2),
(41, 2, 1),
(3, 3, 1),
(4, 3, 1),
(35, 4, 1),
(35, 5, 1),
(35, 6, 2),
(35, 7, 1),
(35, 8, 1),
(35, 9, 2),
(35, 10, 2),
(35, 11, 2),
(35, 12, 2),
(35, 13, 1),
(35, 14, 2),
(3, 15, 1),
(4, 15, 1),
(3, 16, 1),
(4, 16, 1),
(3, 17, 1),
(4, 17, 1),
(3, 18, 2),
(3, 19, 1),
(4, 19, 1),
(3, 20, 2),
(3, 21, 2),
(3, 22, 2),
(3, 23, 2),
(3, 24, 2),
(3, 25, 1),
(3, 26, 2),
(3, 27, 2),
(4, 28, 2),
(4, 29, 2),
(4, 30, 2),
(4, 31, 2),
(4, 32, 2),
(4, 33, 1),
(4, 34, 2),
(4, 35, 2),
(38, 36, 1),
(38, 37, 2),
(38, 38, 1),
(38, 39, 1),
(40, 39, 1),
(38, 40, 2),
(38, 41, 1),
(38, 42, 2),
(38, 43, 2),
(40, 43, 2),
(38, 44, 1),
(38, 45, 2),
(38, 46, 2),
(40, 46, 2),
(38, 47, 1),
(38, 48, 2),
(38, 49, 2),
(41, 50, 1),
(41, 51, 1),
(41, 52, 1),
(41, 53, 1),
(41, 54, 1),
(41, 55, 2),
(41, 56, 2),
(41, 57, 2),
(41, 58, 2),
(41, 59, 2),
(41, 60, 2),
(41, 61, 1),
(41, 62, 2),
(44, 63, 1),
(44, 64, 2),
(44, 65, 2),
(44, 66, 1),
(44, 67, 2),
(44, 68, 1),
(34, 69, 1),
(44, 69, 1),
(44, 70, 1),
(44, 71, 1),
(44, 72, 2),
(44, 73, 2),
(44, 74, 1),
(44, 75, 2),
(44, 76, 2),
(40, 77, 1),
(40, 78, 2),
(40, 79, 1),
(40, 80, 1),
(40, 81, 2),
(40, 82, 2),
(40, 83, 1),
(40, 84, 1),
(40, 85, 2),
(40, 86, 2),
(34, 87, 1),
(34, 88, 1),
(34, 89, 2),
(34, 90, 1),
(34, 91, 2),
(34, 92, 2),
(34, 93, 2),
(34, 94, 2),
(34, 95, 1),
(34, 96, 2),
(34, 97, 1),
(34, 98, 2),
(34, 99, 2),
(36, 100, 2),
(36, 101, 1),
(36, 102, 1),
(36, 103, 2),
(36, 104, 2),
(36, 105, 1),
(36, 106, 2),
(36, 107, 1),
(36, 108, 2),
(36, 109, 1),
(36, 110, 1),
(36, 111, 2),
(3, 112, 1),
(3, 113, 2),
(4, 114, 1),
(4, 115, 2),
(44, 116, 1),
(44, 117, 2),
(34, 118, 1),
(34, 119, 2),
(45, 120, 1),
(42, 121, 2),
(37, 122, 1),
(37, 123, 1),
(37, 124, 1),
(39, 125, 1),
(42, 125, 1),
(43, 126, 1),
(6, 127, 1),
(45, 128, 1),
(42, 129, 2),
(6, 130, 2),
(39, 131, 1),
(42, 131, 1),
(6, 132, 2),
(45, 133, 1),
(45, 134, 1),
(6, 135, 2),
(37, 135, 2),
(39, 135, 2),
(42, 135, 2),
(43, 135, 2),
(45, 135, 2),
(6, 136, 1),
(37, 136, 1),
(39, 136, 1),
(42, 136, 2),
(43, 136, 1),
(45, 136, 1),
(39, 137, 1),
(42, 138, 1),
(42, 139, 2),
(42, 140, 1),
(43, 141, 1),
(39, 142, 1),
(39, 143, 1),
(45, 144, 1),
(43, 145, 1),
(37, 146, 1),
(43, 146, 1),
(6, 147, 2),
(6, 148, 1),
(43, 149, 1),
(43, 150, 1),
(6, 151, 1),
(37, 152, 1),
(37, 153, 1),
(43, 154, 1),
(42, 155, 2),
(39, 156, 1),
(42, 156, 2),
(45, 157, 1),
(42, 158, 1),
(37, 159, 1),
(45, 159, 1),
(6, 160, 1),
(6, 161, 2),
(6, 162, 2),
(6, 163, 1),
(6, 164, 2),
(39, 165, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prueba`
--

DROP TABLE IF EXISTS `prueba`;
CREATE TABLE IF NOT EXISTS `prueba` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `miSet` set('read','write','insert','delete') COLLATE utf8_unicode_ci DEFAULT NULL,
  `miEnum` enum('read','write','insert','delete') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `prueba`
--

INSERT INTO `prueba` (`id`, `miSet`, `miEnum`) VALUES
(1, 'read,write,insert', 'insert'),
(2, 'read', 'read'),
(3, 'read,write', 'read'),
(4, 'read', 'read'),
(5, 'read', 'read'),
(6, 'read', 'read'),
(7, 'read,write', 'read');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resource`
--

DROP TABLE IF EXISTS `resource`;
CREATE TABLE IF NOT EXISTS `resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_resource` (`controller`,`method`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `resource`
--

INSERT INTO `resource` (`id`, `controller`, `method`) VALUES
(1, 'index', 'index');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL,
  `role` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(2, 'admin'),
(1, 'guest'),
(3, 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_resource`
--

DROP TABLE IF EXISTS `role_resource`;
CREATE TABLE IF NOT EXISTS `role_resource` (
  `idRole` int(11) NOT NULL,
  `idResource` int(11) NOT NULL,
  PRIMARY KEY (`idRole`,`idResource`),
  KEY `idRole` (`idRole`),
  KEY `idResource` (`idResource`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `role_resource`
--

INSERT INTO `role_resource` (`idRole`, `idResource`) VALUES
(2, 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `role_resource_all`
--
DROP VIEW IF EXISTS `role_resource_all`;
CREATE TABLE IF NOT EXISTS `role_resource_all` (
`idRole` int(11)
,`idResource` int(11)
,`role` varchar(30)
,`controller` varchar(30)
,`method` varchar(40)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idRole` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idRole` (`idRole`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `password`, `idRole`) VALUES
(3, 'juan', '7e5da5ece8edaa937e431c8600377c91', 3),
(5, 'Rosa', 'e6ae4f9b46303106db282d1df173065f', 3),
(6, 'MarÃ­a', 'd41d8cd98f00b204e9800998ecf8427e', 2),
(7, 'pepe', '926e27eecdbc7a18858b3798ba99bddd', 2);

-- --------------------------------------------------------

--
-- Estructura para la vista `role_resource_all`
--
DROP TABLE IF EXISTS `role_resource_all`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `role_resource_all` AS select `role`.`id` AS `idRole`,`resource`.`id` AS `idResource`,`role`.`role` AS `role`,`resource`.`controller` AS `controller`,`resource`.`method` AS `method` from (`role` join `resource`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `estudio`
--
ALTER TABLE `estudio`
  ADD CONSTRAINT `estudio_ibfk_1` FOREIGN KEY (`idNivel`) REFERENCES `nivel` (`id`);

--
-- Filtros para la tabla `plan`
--
ALTER TABLE `plan`
  ADD CONSTRAINT `plan_ibfk_1` FOREIGN KEY (`idEstudio`) REFERENCES `estudio` (`id`),
  ADD CONSTRAINT `plan_ibfk_2` FOREIGN KEY (`idMateria`) REFERENCES `materia` (`id`);

--
-- Filtros para la tabla `role_resource`
--
ALTER TABLE `role_resource`
  ADD CONSTRAINT `role_resource_ibfk_1` FOREIGN KEY (`idRole`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_resource_ibfk_2` FOREIGN KEY (`idResource`) REFERENCES `resource` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_user_role` FOREIGN KEY (`idRole`) REFERENCES `role` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
