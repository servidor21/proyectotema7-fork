<?php
require_once 'lib/Model.php';

class StudyModel extends Model{
    
    function __construct()
    {
//        echo 'En el UserModel<br>';
        parent::__construct();
    }

    public function delete($id)
    {
        $this->_sql = "DELETE FROM estudio WHERE id=$id";
        $this->executeQuery();
    }

    public function get($id)
    {
        $this->_sql = "SELECT estudio.*, nivel.nivel FROM estudio, nivel"
                . " WHERE estudio.idNivel=nivel.id AND estudio.id = $id ORDER BY id";
        $this->executeSelect();
        return $this->_rows[0];
    }

    public function getFromLevel($idLevel)
    {
        $this->_sql = "SELECT estudio.*, nivel.nivel FROM estudio, nivel"
                . " WHERE estudio.idNivel=nivel.id AND estudio.idNivel = $idLevel ORDER BY id";
        $this->executeSelect();
        return $this->_rows;
    }

        public function getAll()
    {
        $this->_sql = "SELECT estudio.*, nivel.nivel FROM estudio, nivel"
                . " WHERE estudio.idNivel=nivel.id"
                . " ORDER BY id";
        $this->executeSelect();
        return $this->_rows;
    }

    public function insert($fila)
    {
        $this->_sql = "INSERT INTO estudio(codInterno, nombre, idNivel, codOficial) "
                . "VALUES ('$fila[codinterno]', '$fila[nombre]', '$fila[idNivel]', '$fila[codOficial]')";

        $this->executeQuery();
    }

    public function update($row)
    {
        $this->_sql = "UPDATE estudio SET "
                . " codInterno='$row[codinterno]', "
                . " nombre='$row[nombre]',"
                . " idNivel=$row[idNivel],"
                . " codOficial='$row[codOficial]'"
                . " WHERE id = $row[id]";
        $this->executeQuery();
    }

}